
  //getting value from input text
 function  getInputValue(elementID: string): string {
    let inputElement: HTMLInputElement = <HTMLInputElement>(
      document.getElementById(elementID)
    );
    console.log("elementID");
    console.log(elementID);
    return inputElement.value;
  }

  function logger (message: string) : void {
      console.log(message);
  }

  export {  getInputValue as getValue, logger  } ;

