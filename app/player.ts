import {IPerson  } from './person';


export class Player implements IPerson {
  name: string;
  age: number;
  highScore: number;

  formatName() {
    return this.name.toUpperCase();
  }
}


