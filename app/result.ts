export interface IResult {
    //members of the interface
    playerName : string;
    score: number;
    problemCount: number;
    factor: number;
    logger ?: (value: string) => void;

}